#!/bin/bash

set -e

PRIVATE_KEY=$PRIVATE_KEY
eval $(ssh-agent -s)
echo"$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

./disableHostKeyChecking.ssh


DEPLOY_SERVERS=$DEPLOY_SERVERS

echo "deploying to $DEPLOY_SERVERS"
ssh ubuntu@DEPLOY_SERVERS 'bash' < ./updateAndRestart.sh
